var http = require('http');

exports.handler = (event, context) => {

  try {

    if (event.session.new) {
      // New Session
      console.log("NEW SESSION")
    }

    switch (event.request.type) {

      case "LaunchRequest":
      // Launch Request
      console.log(`LAUNCH REQUEST`)
      context.succeed(
        generateResponse(
          buildSpeechletResponse("Welcome to your first Alexa Skill. You can start by asking for what's the weather like.", true),
          {}
        )
      )
      break;

      case "IntentRequest":
      // Intent Request
      console.log(`INTENT REQUEST`)
      console.log('intent name is ', event.request.intent.name);
      // http://m.fmi.fi/mobile/interfaces/weatherdata.php?locations=636242&l=fi&version=1.1.10
      var headers = {'accept-encoding': 'none', 'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.89 Safari/537.36'}
      var options = {'hostname': 'm.fmi.fi', 'path': '/mobile/interfaces/weatherdata.php?locations=636242&l=fi&version=1.1.10', 'headers': headers};
      var body = ""
      
      switch(event.request.intent.name) {

        case "GetGeneralWeather":
        http.get(options, (response) => {
          response.on('data', (chunk) => { body += chunk });
          response.on('end', () => {
            let weatherData = JSON.parse(body)
            let time_now = new Date();
            let UTCString = getUTCString(time_now);
            let forecast;
            for (forecast of weatherData['forecasts'][0]['forecast']){
              if(forecast.utctime === UTCString){
                //console.log('forecast is ', forecast);
                let temperature_now = forecast.Temperature;
                let weather_symbol = forecast.WeatherSymbol3;
                let weather_symbol_text = getDescriptionForWeatherSymbol(weather_symbol);
                let feels_like = forecast.FeelsLike;
                let wind_speed = forecast.WindSpeedMS;
                let wind_text = getWindText(wind_speed);
                let weather_description = `Weather in Helsinki is ${temperature_now} degrees, feels like ${feels_like} degrees. ${weather_symbol_text}, ${wind_text} with ${wind_speed} metres per second. `;
                context.succeed(
                  generateResponse(
                    buildSpeechletResponse(weather_description, true),
                    {}
                  )
                )
              }
            }
          })
        }).on('error', (e) => {
          console.log('error is ', e.message)
        })
        break;
        
        case "GetWind":
        http.get(options, (response) => {
          response.on('data', (chunk) => { body += chunk });
          response.on('end', () => {
            let weatherData = JSON.parse(body)
            let time_now = new Date();
            let UTCString = getUTCString(time_now);
            let forecast;
            for (forecast of weatherData['forecasts'][0]['forecast']){
              if(forecast.utctime === UTCString){
                console.log('forecast is ', forecast);
                let wind_speed = forecast.WindSpeedMS;
                let wind_speed_text = getWindText(wind_speed);
                let wind_direction = forecast.WindCompass;
                let wind_direction_text = getWindCompassText(wind_direction);
                let wind_description = `Wind is now ${wind_speed_text} with ${wind_speed} metres per second blowing from ${wind_direction_text}. `;
                context.succeed(
                  generateResponse(
                    buildSpeechletResponse(wind_description, true),
                    {}
                  )
                )
              }
            }
          })
        }).on('error', (e) => {
          console.log('error is ', e.message)
        })
        break;

        case "GetRain":
        http.get(options, (response) => {
          response.on('data', (chunk) => { body += chunk });
          response.on('end', () => {
            let weatherData = JSON.parse(body)
            let time_now = new Date();
            let UTCString = getUTCString(time_now);
            let forecast;
            for (forecast of weatherData['forecasts'][0]['forecast']){
              if(forecast.utctime === UTCString){
                console.log('forecast is ', forecast);
                let precipitation = forecast.Precipitation1h;
                let weather_symbol = forecast.WeatherSymbol3;
                let rain_text = 'It is not raining in Helsinki';
                if(precipitation > 0.1 || getRainTextForWeatherSymbol(weather_symbol)){
                  rain_text = 'Currently, there is ' + getRainTextForWeatherSymbol(weather_symbol) + (precipitation > 0.1 ? (+ ' with ' + precipitation + ' millilitres') : '') + '.';
                }
                context.succeed(
                  generateResponse(
                    buildSpeechletResponse(rain_text, true),
                    {}
                  )
                )
              }
            }
          })
        }).on('error', (e) => {
          console.log('error is ', e.message)
        })
        break;
  
        default:
        throw "Invalid intent"
      }
      
      break;

      case "SessionEndedRequest":
      // Session Ended Request
      console.log(`SESSION ENDED REQUEST`)
      break;

      default:
      context.fail(`INVALID REQUEST TYPE: ${event.request.type}`)

    }

  } catch(error) { context.fail(`Exception: ${error}`) }

}

// Helpers
buildSpeechletResponse = (outputText, shouldEndSession) => {

  return {
    outputSpeech: {
      type: "PlainText",
      text: outputText
    },
    shouldEndSession: shouldEndSession
  }

}

generateResponse = (speechletResponse, sessionAttributes) => {

  return {
    version: "1.0",
    sessionAttributes: sessionAttributes,
    response: speechletResponse
  }

}

getUTCString = (time_now) => {
  let UTC_year = time_now.getUTCFullYear();
  let UTC_month = time_now.getUTCMonth();
  let real_UTC_month = UTC_month + 1;
  let UTC_date = time_now.getUTCDate();
  let UTC_hours_now = time_now.getUTCHours();
  let UTCString = '' + UTC_year + (real_UTC_month.toString().length == 1 ? '0' : '') + real_UTC_month + UTC_date + 'T' + (UTC_hours_now.toString().length == 1 ? '0' : '') + UTC_hours_now + '0000';
  return UTCString;
}

getWindText = (wind) => {
  if (wind == 0) return "calm"
  if (wind <= 2) return "light air"
  if (wind <= 3) return "light breeze"
  if (wind <= 5) return "gentle breeze"
  if (wind <= 8) return "moderate breeze"
  if (wind <= 11) return "fresh breeze"
  if (wind <= 14) return "strong breeze"
  if (wind <= 17) return "moderate gale"
  if (wind <= 20) return "fresh gale"
  if (wind <= 24) return "strong gale"
  else return "storm"
}

getWindCompassText = (wind_compass) => {
  if (wind_compass == 'N') return 'north'
  if (wind_compass == 'E') return 'east'
  if (wind_compass == 'W') return 'west'
  if (wind_compass == 'S') return 'south'
  if (wind_compass == 'NE') return 'northeast'
  if (wind_compass == 'SE') return 'southeast'
  if (wind_compass == 'NW') return 'northwest'
  if (wind_compass == 'SW') return 'southwest'
}

getDescriptionForWeatherSymbol = (symbolId) => {
  switch (symbolId){
    case "1": return "sunny";
    case "2": return "partly cloudy";
    case "21": return "light showers"
    case "22": return "showers"
    case "23": return "heavy showers"
    case "3": return "cloudy"
    case "31": return "light rain"
    case "32": return "rain"
    case "33": return "heavy rain"
    case "41": return "light snow showers"
    case "42": return "snow showers"
    case "43": return "heavy snow showers"
    case "51": return "light snowfall"
    case "52": return "snowfall"
    case "53": return "heavy snowfall"
    case "61": return "thundershowers"
    case "62": return "heavy thundershowers"
    case "63": return "thunder"
    case "64": return "heavy thunder"
    case "71": return "light sleet showers"
    case "72": return "sleet showers"
    case "73": return "heavy sleet showers"
    case "81": return "light sleet"
    case "82": return "sleet"
    case "83": return "heavy sleet"
    case "91": return "moderate fog";
    case "92": return "heavy fog";
    default: return;
  }
}

getRainTextForWeatherSymbol = (symbolId) => {
  switch (symbolId){
    case "21": return "light showers"
    case "22": return "showers"
    case "23": return "heavy showers"
    case "31": return "light rain"
    case "32": return "rain"
    case "33": return "heavy rain"
    case "41": return "light snow showers"
    case "42": return "snow showers"
    case "43": return "heavy snow showers"
    case "51": return "light snowfall"
    case "52": return "snowfall"
    case "53": return "heavy snowfall"
    case "61": return "thundershowers"
    case "62": return "heavy thundershowers"
    case "63": return "thunder"
    case "64": return "heavy thunder"
    case "71": return "light sleet showers"
    case "72": return "sleet showers"
    case "73": return "heavy sleet showers"
    case "81": return "light sleet"
    case "82": return "sleet"
    case "83": return "heavy sleet"
    default: return;
  }
}
